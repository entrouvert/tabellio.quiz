from five import grok
from Acquisition import aq_inner
from zope.interface import implements

from zope import interface
from zope import schema

from zope.component import getMultiAdapter, provideAdapter
from Products.CMFCore.utils import getToolByName

from z3c.form import form, field, button, validator
from z3c.form.browser.radio import RadioFieldWidget

from plone.z3cform.layout import wrap_form
from z3c.form.ptcompat import ViewPageTemplateFile
from plone.dexterity.content import Item

from plone.formwidget.captcha.widget import CaptchaFieldWidget
from plone.formwidget.captcha.validator import CaptchaValidator

from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary

from tabellio.quiz.interfaces import MessageFactory as _


class IQuizForm(interface.Interface):
    title = schema.TextLine(title=_(u'Title'))
    description = schema.Text(title=_(u'Description'), required=False)
    questions = schema.Text(title=_(u'Questions'))

class QuizForm(Item):
    implements(IQuizForm)

class View(grok.View):
    grok.context(IQuizForm)
    grok.require('zope2.View')

    def quiz_form(self):
        effective_quiz = EffectiveQuiz(self.context)
        effective_form = EffectiveQuizForm(effective_quiz, self.request)
        effective_form.update()
        return effective_form.render()

class EffectiveQuiz(object):
    def __init__(self, context):
        self.context = context

    def absolute_url(self):
        return self.context.absolute_url()


class EffectiveQuizForm(form.Form):

    @property
    def fields(self):
        list_of_fields = []
        self.good_answers = {}
        for i, question in enumerate(self.context.context.questions.split('--')):
            question_lines = question.strip().splitlines()
            if not question_lines:
                continue
            question_title = question_lines[0]
            question_answers = [x[2:] for x in question_lines[1:]]
            self.good_answers['q%s' % (i+1)] = [x[2:] for x in question_lines[1:] if x.startswith('o')]
            terms = [SimpleVocabulary.createTerm(x, x.encode('ascii', 'ignore'), x)
                     for x in question_answers]
            question_vocabulary = SimpleVocabulary(terms)
            question_field = schema.Choice(title=question_title,
                                           vocabulary=question_vocabulary,
                                           required=False)
            question_field.__name__ = 'q%s' % (i+1)
            question_field.widgetFactory = RadioFieldWidget
            list_of_fields.append(question_field)
        fields = field.Fields(*list_of_fields)
        for key in fields._data.keys():
            fields[key].widgetFactory = RadioFieldWidget
        return fields

    template = ViewPageTemplateFile('form_templates/view_effectivequiz.pt')

    @button.buttonAndHandler(_(u'Check my Answers'))
    def handleApply(self, action):
        return

    def was_submitted(self):
        if self.request.form:
            return True
        return False

    def is_correct(self):
        data, errors = self.extractData()
        error = False
        for key in self.fields._data.keys():
            if [data.get(key)] != self.good_answers[key]:
                return False
        return True

    def get_score(self):
        data, errors = self.extractData()
        error = False
        score = 0
        for key in self.fields._data.keys():
            if [data.get(key)] == self.good_answers[key]:
                score += 1
        return score

